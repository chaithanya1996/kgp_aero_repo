---
title: "Aerodynamics"
date: 2018-05-12T19:40:21+05:30
draft: false
---
# Aerodynamics

## Summary Sheets

1. {{<storagefile "files/aerodynamics/aerodynamicsAFullVersion.pdf" "Aerodynamics - A">}} 
1. {{<storagefile "files/aerodynamics/aerodynamicsBFullVersion.pdf" "Aerodynamics - B">}} 
1. {{<storagefile "files/aerodynamics/aerodynamicsCFullVersion.pdf" "Aerodynamics - C">}} 

## Formula Sheet

1. {{<storagefile "files/aerodynamics/aerodynamicsFormulaOverview.pdf" "Aerodynamics Formula sheet">}} 