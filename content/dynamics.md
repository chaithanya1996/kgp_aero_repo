---
title: "Dynamics"
date: 2018-05-14T22:54:36+05:30
draft: false
---

## Dynamics Study Notes

1. {{<storagefile "files/dynamics/BristolDynamicsReader1.pdf" "Part 1">}}
2. {{<storagefile "files/dynamics/BristolDynamicsReader2.pdf" "Part 2">}}
3. {{<storagefile "files/dynamics/BristolDynamicsReader3.pdf" "Part 3">}}


## Problem Solving Guides

5. {{<storagefile "files/dynamics/Dynamics_summary.pdf" "Problems and sumamry">}}
6. {{<storagefile "files/dynamics/problemSolvingGuide.pdf" "Problem Solving Guide">}}

## AstroDynamics Summary

1. {{<storagefile "files/dynamics/AstrodynamicsSummary.pdf" "Astrodynamics Part-1">}}
2. {{<storagefile "files/dynamics/Astrodynamics.pdf" "Astrodynamics Part-2">}}
