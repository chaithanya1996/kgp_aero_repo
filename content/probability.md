---
title: "Probability and Statistics"
date: 2018-05-12T19:40:21+05:30
draft: false
---
# Probability and statistics

## Summary Sheets

1. {{< storagefile "files/probability/probabilityTheoryFullVersion.pdf" "Probability and Statistics Intro Sheet">}} 


## Formula Sheet

1. [Modern Intro To Probability and Statistics](https://cis.temple.edu/~latecki/Courses/CIS2033-Spring13/Modern_intro_probability_statistics_Dekking05.pdf)