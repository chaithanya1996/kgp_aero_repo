---
title: "Aerospace Structural Dynamics"
date: 2018-04-17T07:23:32+05:30
draft: false
---

# Aerospace Structural Dynamics

1. {{< storagefile "files/structural_dynamics/03StructuralDynamicsIntro.pdf" "Structural Dynamics Intro">}}
2. {{< storagefile "files/structural_dynamics/04StructuralDynamicsSDOF.pdf" "Structural Dynamics SDOF 1">}}
3. {{< storagefile "files/structural_dynamics/05StructuralDynamicsSDOF.pdf" "Structural Dynamics SDOF 2">}}
4. {{< storagefile "files/structural_dynamics/06StructuralDynamicsSDOF.pdf" "Structural Dynamics SDOF 3">}}
5. {{< storagefile "files/structural_dynamics/07StructuralDynamicsCoulombDamping.pdf" "Structural Dynamics Coulomb Damping">}}
6. {{< storagefile "files/structural_dynamics/08ForcedVibration.pdf" "Forced Vibration 1">}}
7. {{< storagefile "files/structural_dynamics/09ForcedVibration.pdf" "Forced Vibration 2">}}
8. {{< storagefile "files/structural_dynamics/09ForcedVibration.pdf" "Forced Vibration 3">}}
9. {{< storagefile "files/structural_dynamics/10ForcedVibration.pdf" "Forced Vibration 4">}}
10. {{< storagefile "files/structural_dynamics/11VibrationInstruments.pdf" "Vibration Instruments">}}
11. {{< storagefile "files/structural_dynamics/12energyDissipated.pdf" "Energy Dissipated">}}
12. {{< storagefile "files/structural_dynamics/13forcedResponse.pdf" "Forced Response">}}
13. {{< storagefile "files/structural_dynamics/13forcedResponse2.pdf" "Forced Response 2">}}
14. {{< storagefile "files/structural_dynamics/14laplaceTransformation.pdf" "laplace Transformation">}}
15. {{< storagefile "files/structural_dynamics/15twoDOFS.pdf" "Two DOFS">}}
16. {{< storagefile "files/structural_dynamics/16coordinateCouplingBeating.pdf" "Coordinate Coupling Beating">}}
17. {{< storagefile "files/structural_dynamics/17response2dosf.pdf" "Response of Two DOF">}}
18. {{< storagefile "files/structural_dynamics/18modalAnalysis.pdf" "Modal Analysis">}}
19. {{< storagefile "files/structural_dynamics/19continuousSystem.pdf" "Continuous System ">}}
20. {{< storagefile "files/structural_dynamics/20continuousSystemBeam1.pdf" "continuous SystemBeam 1">}}
21. {{< storagefile "files/structural_dynamics/21continuousSystemBeam2.pdf" "continuous SystemBeam 2">}}
22. {{< storagefile "files/structural_dynamics/22continuousSystemBeam3.pdf" "continuous SystemBeam 3">}}
23. {{< storagefile "files/structural_dynamics/23continuousSystemBeam3.pdf" "continuous SystemBeam 4">}}
24. {{< storagefile "files/structural_dynamics/24genCoordinate.pdf" "General Coordinates">}}
25. {{< storagefile "files/structural_dynamics/25genCoordinate2.pdf" "General Coordinates">}}
26. {{< storagefile "files/structural_dynamics/26rayleighsMethod.pdf" "Rayleighs Method">}}
27. {{< storagefile "files/structural_dynamics/27rayleighsMethod2.pdf" "Rayleighs Method 2">}}
28. {{< storagefile "files/structural_dynamics/28nonlinearResponse.pdf" "Non linear Response">}}


## Turorials

1. {{< storagefile "files/structural_dynamics/StructuralDynamicsP01Q.pdf" "Tutorial - 1 ">}}
2. {{< storagefile "files/structural_dynamics/StructuralDynamicsP02Q.pdf" "Tutorial - 2 ">}}
3. {{< storagefile "files/structural_dynamics/StructuralDynamicsP03Q.pdf" "Tutorial - 3 ">}}
4. {{< storagefile "files/structural_dynamics/StructuralDynamicsP04Q.pdf" "Tutorial - 4 ">}}
