---
title: "Experimental Stress Analysis"
date: 2018-08-03T07:23:32+05:30
draft: false
---

# Experimental Stress Analysis Chapters

1. {{< storagefile "files/ESA/Chapter1.pdf" "Chapter 1">}}
2. {{< storagefile "files/ESA/Chapter2.pdf" "Chapter 2">}}
3. {{< storagefile "files/ESA/Chapter3.pdf" "Chapter 3">}}
4. {{< storagefile "files/ESA/Chapter5.pdf" "Chapter 5">}}
5. {{< storagefile "files/ESA/Chapter6.pdf" "Chapter 6">}}
6. {{< storagefile "files/ESA/Chapter8.pdf" "Chapter 8">}}
7. {{< storagefile "files/ESA/Chapter10.pdf" "Chapter 10">}}
8. {{< storagefile "files/ESA/Chapter11.pdf" "Chapter 11">}}
9. {{< storagefile "files/ESA/Chapter13.pdf" "Chapter 13">}}
10. {{< storagefile "files/ESA/Chapter14.pdf" "Chapter 14">}}

# Book

1. [ANALYSIS AND PERFORMANCE OF FIBER COMPOSITES BY BHAGWAN D. AGARWAL](https://soaneemrana.org/onewebmedia/ANALYSIS%20AND%20PERFORMANCE%20OF%20FIBER%20COMPOSITES%20BY%20BHAGWAN%20D.%20AGARWAL.pdf)
2. {{< storagefile "files/ESA/Analysis-and-performance-of-fiber-composites-solutions-manual.pdf" "Analysis and performance of fiber composites solutions manual">}}

## Assignment

1. {{< storagefile "files/ESA/assignment.pdf" "Assignment 1">}}
2. {{< storagefile "files/ESA/ESA_example_problem_nos.pdf" "Practice Problem No">}}
3. {{< storagefile "files/ESA/ESA_example_problem_solutions.pdf" "Example Problems">}}
4. {{< storagefile "files/ESA/four_point_bending.pdf" "Four Point Bending(Incomplete)">}}

The Example Problems for practive are from **ANALYSIS AND PERFORMANCE OF FIBER COMPOSITES**  3rd ***Edition***

## Assignment  Solutions

1. {{<storagefile "files/ESA/assignmnet_1.pdf" "Assignment 1 (Better)">}}
1. {{<storagefile "files/ESA/ESA_ass_2.pdf" "Assignment 2">}}



