---
title: "Aeroelasticity"
date: 2019-02-20T15:04:45+05:30
draft: false
---

# Book

- [INTRODUCTION TO AIRCRAFT AEROELASTICITY AND LOADS](https://onlinelibrary.wiley.com/doi/pdf/10.1002/9781118700440)

You can only Acess Above Book From Institute Network.

# Assignment 

1. {{<storagefile "files/aeroelasticity/submission_1.pdf" "Assignment 1">}}
