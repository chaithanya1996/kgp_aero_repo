---
title: "Calculus"
date: 2018-04-22T07:23:32+05:30
draft: false
---

## Summary

1. {{<storagefile "files/calculus/CalculusWI1401LRSummary.pdf" "Summary Sheet - 1">}}
2. {{<storagefile "files/calculus/summaryOfFormulas1.pdf" "Summary Sheet - 2">}}
3. {{<storagefile "files/calculus/summaryOfFormulas2.pdf" "Summary Sheet - 3">}}
4. {{<storagefile "files/calculus/summaryPart1.pdf" "Summary Sheet - 4">}}
5. {{<storagefile "files/calculus/summaryPart2.pdf" "Summary Sheet - 5">}}
6. {{<storagefile "files/calculus/differentialEquationsFullVersion.pdf" "Summary of Differential Equations">}}

## Exams

- [Calculus Exam from University of Bristol](http://www.aerostudents.com/courses/calculus-1-a/BristolEngineeringMathematics2010.pdf)
- [Solution to calculus Exam from University of Bristol](http://www.aerostudents.com/courses/calculus-1-a/BristolEngineeringMathematics2010Solutions.pdf)
## Links

- [Khan Acdemy Course](https://www.khanacademy.org/math/differential-calculus)
- [MIT Open Courses](https://ocw.mit.edu/courses/#mathematics)


The Above course files are sourced from [aerostudents](http://www.aerostudents.com/courses/calculus-1-a/calculus-1-a.php)