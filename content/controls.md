---
title: "Controls"
date: 2018-05-14T23:26:29+05:30
draft: false
---
## systems Theory

1. {{< storagefile "files/controls/systemsTheoryFullVersion.pdf" "Systems Sumamry">}} 

## Sumamry


1. {{< storagefile "files/controls/controlTheoryFullVersion.pdf" "Control Theory Sumamry">}} 
2. {{< storagefile "files/controls/SatelliteDataProcessingSummary.pdf" "Satellite Data Processing">}}
3. {{< storagefile "files/controls/automaticFlightControlFullVersion.pdf" "Automatics Flight Control">}}
4. {{< storagefile "files/controls/nonlinearDynamicInversion.pdf" "Non Linear Dynamics Inversion">}}
5. {{< storagefile "files/controls/backstepping.pdf" "Backstepping">}}
