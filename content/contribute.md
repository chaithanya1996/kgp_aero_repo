---
title: "Contribute"
date: 2018-04-17T03:45:02+05:30
draft: false
---

# Contributing to Repository

This Repository is Hostd using [GitLab's](https://gitlab.com) Pages and built using [Hugo](https://gohugo.io) Static Site Generator.This site is just is just mere collection of Resources sourced from students and Various parts of internet. Everyone is Welcome To comtribute Resources. There are many ways to contribute

## Contributing Resouces

You can send Resources like Course Hand written summaries to contribute[at]iitkgp[dot]men. Your contributions are always properly credited to you.
A Direct Upload page is in works and made available as soon as possible.

## Contributing Directly To Repository

If You are are familiar with static site genrators like hugo jekyll and others which allow writing blog post and based on the Markdown
 you can for fork sites main Repository [**here**](https://gitlab.com/chaithanya1996/kgp_aero_repo) and Open a pull request. Also you can mail a blog post or for course page written in markdown to contribute[at]iitkgp[dot]men.

 Here is a example  {{<storagefile "files/markdown_example.md" "Markdown file">}}. Here is rendered output of the [file](https://gist.github.com/rt2zz/e0a1d6ab2682d2c47746950b84c0b6ee).You can visit [this](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) for quick reference. 

 Thank you for Your contribution.

