---
title: "Flight Testing Laboratory"
date: 2018-04-17T07:23:32+05:30
draft: false
---

## Experiment Reports

1. {{< storagefile "files/AE49012/1_D_KGP.pdf" "Calculation of C.G">}}
2. {{< storagefile "files/AE49012/2_D_KGP.pdf" "Calibration of control surfaces">}}
3. {{< storagefile "files/AE49012/3_D_KGP.pdf" "Drag Polar Estimation using Cessna-206H">}}
4. {{< storagefile "files/AE49012/4_D_KGP.pdf" "Determination of Neutral Point and Maneuvering Point from Flight Tests">}}

- {{< storagefile "files/AE49012/Report_D_KGP.pdf" "Final Report">}}