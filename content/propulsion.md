---
title: "Propulsion"
date: 2018-05-12T21:25:42+05:30
draft: false
---

# Thermodynamics

1. {{< storagefile "files/propulsion/thermodynamicsFullVersion.pdf" "Thermodynamics Summary 1">}}
2. {{< storagefile "files/propulsion/physics1Summary.pdf" "Thermodynamics Summary 2">}}

# Propulsion

- [Propulsion Notes](http://www.aerostudents.com/courses/propulsion-and-power/PropulsionAndPowerSummary.pdf)