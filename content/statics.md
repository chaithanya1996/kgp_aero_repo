---
title: "Statics"
date: 2018-05-10T20:44:29+05:30
draft: false
---
# Summarised Resources

1. {{< storagefile "files/statics/staticsDefinitions.pdf" "Definitions">}}
2. {{< storagefile "files/statics/staticsFullVersion.pdf" "Problem Solving Techniques">}}

# Book

- [Statics Book](http://www.aerostudents.com/courses/statics/EngineeringMechanicsEquilibrium.pdf)