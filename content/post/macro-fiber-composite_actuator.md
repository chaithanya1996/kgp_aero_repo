---
title: "Macro Fiber Composite Actuator"
date: 2018-03-29T04:28:30+05:30
draft: false
mathjax : true
---
# Actuation of Structures with PZT
## Piezoelectricity
Piezoelectricity is the property of certain crystalline materials to develop electric charge in response to applied mechanical stress. Piezoelectric materials have the unique ability to interchange electrical energy and mechanical strain energy or force. Due to this characteristic of the material, it has been found to be very effective for use in dynamic applications involving vibration suppression and sensing. These materials have been used in numerous other applications including sonar, ultrasonic transducers (e.g. television remote controls) and piezo ceramic ignition systems Piezoelectric materials exhibit electromechanical coupling, which is useful for the design of devices for sensing and actuation. 

&nbsp;The coupling is exhibited in the fact that piezoelectric materials produce an electrical displacement when a mechanical stress is applied and can produce mechanical strain under the application of an electric field. Due to the fact that the mechanical-to-electrical coupling was discovered first, this property is termed the direct effect, while the electrical-to-mechanical coupling is termed the converse piezoelectric effect.

## Modelling Piezoelectricity
For linear piezoelectric materials, the interaction between the electrical and mechanical variables can be described by linear relations. A constitutive relation is established between mechanical and electrical variables in the tensorial form.

### Actuation Equations

$$
  \begin{align}
  S _{ij} = s _{ijkl} ^D T _{kl} + g _{kij} D _{k} \newline
  D _i = d _{ikl} T _{kl} + \epsilon ^T _{ik} E _{k}
  \end{align}
$$

where $S _{ij}$ and $T _{ij}$ are the strain and stress, $E$, $k$, and $D$,$i$ are the electric field and electric
displacementThe stress and strain variables are second-order tensors, whereas the electric field and the electric displacement are first-order tensors.

The coefficient $s _{ijkl} ^E$ is the compliance, which signifies the strain per unit stress. The coefficient $\epsilon ^T _{ik}$ is the electric permittivity, which signifies electric displacement per unit electric field. The coefficients d ikl and d kij signify the coupling between the electrical and the mechanical variables, i.e., the charge per unit stress and the strain per unit electric field.

### Sensing Equations
The above equations can be replaced by an equivalent set of equations that highlight the sensing effect, i.e., predict how much electric field will be generated by a given state of stress, electric displacement. (As the electric voltage is directly related to the electric field, this arrangement is preferred for sensing applications.) 
$$
  \begin{align}
  S _{ij} = s _{ijkl} ^E T _{kl} + d _{kij} E _{k} \newline
  E _i = g _{ikl} T _{kl} + \beta ^T _{ik} D _{k}
  \end{align}
$$

The coefficient $g _{ikl}$ is the piezoelectric voltage coefficient, and represents how much electric field
is induced per unit stress. The coefficient $\beta ^T _{ik}$ is the impermittivity coefficient.
## Modelling Macro Fiber Composite Actuation on a Rectangular Plate

