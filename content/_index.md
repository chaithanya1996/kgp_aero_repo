---
title: Home
---

[<img src="/images/kgp_logo.png" style="max-width:15%;min-width:40px;float:right;" alt="KGP LOGO" />](https://aero.iitkgp.men)

# Aerospace File Repository

This is a repository of files for courses taught and also curated list of useful Resources.  
This is the navigation page for all the Departmental and Miscellenious courses.  
Previous year question papers and Blog posts on topics can be found in thier respective pages which are linked below in the respective section

<hr/>
## Structures

- [Vibrations (External)](/vibrations)
- [Structural Dynamcis (AE31002)](/ae31002)
- [Advanced Structural Dynamics (AE40004)](/ae40004)
- [Smart Structures (AE51019)](/ae51019)
- [Composite Structures (AE40006)](/ae40006)
- [Finite Element Method (AE40033)](/ae40033)
- [Statics (External)](/statics)
- [Dynamics (External)](/dynamics)
- [Materials (External)](/materialsext)
- [Structures (External)](/structures_intro)
- [Experimental Stress Analysis](/esa)
- [Design of Aircraft Components using Composite Materials](/cdkong)
- [Aeroelasticity](/aeroelasticity)

<hr/>
## Controls

- [Flight Testing Laboratory (AE49012)](/ae49012)
- [Orbital Flight Mechanics (External)](/flightmechanics)
- [Control Theory (External)](/controls)
- [Dynamics and Stability (External)](/stability)

<hr/>
## Aerodynamics

- [Aerodynamics (External)](/aerodynamics)
- [Introduction to Aerospace (External)](/intro_to_aerospace)
- [Computational Fluid Dynamics (External)](/cfd)
- [Constitutive Modelling (External)](/constitutive_modelling)

<hr/>
## Propulsion

- [Propulsion and Power (External)](/propulsion)
- [Combustion in Jet Engines (AE51007)](/ae51007)
- [Advanced Gas Turbine Theory (AE40030)](/gasturb)
- [Rocket Propulsion](/rocket)

<hr/>

## Miscellaneous Course files

The Below Links are Sourced from aerostudents.com 

- [Calculus (External)](/calculus)
- [Statics (External)](/statics)
- [Materials (External)](/materialsext)
- [Aerodynamics (External)](/aerodynamics)
- [Probability and Statistics (External)](/probability)
- [Structures (External)](/structures_intro)
- [Vibrations (External)](/vibrations)
- [Propulsion and Power (External)](/propulsion)
- [Orbital Flight Mechanics (External)](/flightmechanics)
- [Introduction to Aerospace (External)](/intro_to_aerospace)
- [Dynamics (External)](/dynamics)
- [Computational Fluid Dynamics (External)](/cfd)
- [Constitutive Modelling (External)](/constitutive_modelling)
- [Control Theory (External)](/controls)
- [Dynamics and Stability (External)](/stability)
- [Advanced Aircraft Design](/design)


## [Contribution guide](/contribute)


<hr/>


# Blog Posts
