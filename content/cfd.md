---
title: "Computational Fluid Dynamics"
date: 2018-05-14T23:27:05+05:30
draft: false
---

1. {{< storagefile "files/cfd/computationalFluidAndSolidMechanicsFullVersion.pdf" "Computational Fluids and Solid Mechanics">}} 
2. {{< storagefile "files/cfd/thePanelMethod.pdf" "Panel Method">}} 
3. {{< storagefile "files/cfd/hyperbolicAndEllipticEquations.pdf" "Hyperbolic and eliptics Equations">}} 
4. {{< storagefile "files/cfd/timeDependentProblems.pdf" "Time Dependent Problems">}} 
5. {{< storagefile "files/cfd/approximationsAndTheirErrors.pdf" "Approximations and Thier Errors">}} 


