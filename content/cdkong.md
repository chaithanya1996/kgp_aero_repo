---
title: "Design of Aircraft Components using Composite Materials"
date: 2019-01-17T10:32:52+05:30
draft: false
---

# Lecture Notes 

1. {{< storagefile "files/cdkong/1.pdf" "Lecture 1">}}
2. {{< storagefile "files/cdkong/2.pdf" "Lecture 2">}}
3. {{< storagefile "files/cdkong/3.pdf" "Lecture 3">}}
4. {{< storagefile "files/cdkong/4.pdf" "Lecture 4">}}
5. {{< storagefile "files/cdkong/5.pdf" "Lecture 5">}}
